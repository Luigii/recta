/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recta;

/**
 *
 * @author LUIS
 */
public class Datos {
    private float x1;
    private float x2;
    private float y1;
    private float y2;

    public Datos() {
    }

    public float getX1() {
        return x1;
    }

    public void setX1(float x1) {
        this.x1 = x1;
    }

    public float getX2() {
        return x2;
    }

    public void setX2(float x2) {
        this.x2 = x2;
    }

    public float getY1() {
        return y1;
    }

    public void setY1(float y1) {
        this.y1 = y1;
    }

    public float getY2() {
        return y2;
    }

    public void setY2(float y2) {
        this.y2 = y2;
    }

  
    
    
   public float CalcularPendiente()
   {
   
           
           
       return (y1 - y2)/(x1 - x2) ;
       
       
   }
    
}
