/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recta;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author LUIS
 */
public class Controlador implements ActionListener{
    
    private Datos modelo;
    private Vista vista;

    public Controlador(Datos modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
    }
    
    public void iniciar() {
        vista.setTitle("Calculadora");//Titulo de la ventana
        vista.setLocationRelativeTo(null);//Ubicar en el centro de la pantalla
        vista.setVisible(true);//Mostrar
        asignarControl();
    }

    public void Calcular() {

        float X1 =Float.parseFloat(vista.getx1().getText());
        float X2 =Float.parseFloat(vista.getx2().getText());
        float Y1 =Float.parseFloat(vista.gety1().getText());
        float Y2 =Float.parseFloat(vista.gety2().getText());
        
        modelo.setX1(X1);
        modelo.setX2(X2);
        modelo.setY1(Y1);
        modelo.setY2(Y2);
        

    }

    private void asignarControl() {
        
        vista.getBtnCalcular().addActionListener(this);
        vista.getLimpiar().addActionListener(this);
        
    }
    
    public void Limpiar()
    {
    vista.getx1().setText("");
        vista.getx2().setText("");
        vista.gety1().setText("");
        vista.gety2().setText("");
        vista.getResult().setText("");
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if (ae.getSource() == vista.getLimpiar()){Limpiar();}
        
        if (ae.getSource() == vista.getBtnCalcular()){
            Calcular();
        float res = modelo.CalcularPendiente();
        
       vista.getResult().setText(""+res );
        
        }
        
    }
    
    
}
